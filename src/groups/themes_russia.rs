use crate::utils::mention_user;
use tbot::types::{chat, User};

pub const ID: chat::Id = chat::Id(-1001351743135);

pub fn welcome(is_plural: bool) -> String {
    format!(
        "*Привет!* Рады видеть {see_you} в русскоязычной группе по темам \
        для Телеграма.\n\n\

        Если {you_want} научиться создавать темы, но не {do_not_know} как, \
        то {read} [эту статью]\
        (http://telegra.ph/Polnyj-gajd-po-temam-dlya-Telegrama-na-Android-12-16). \
        Также {you_need} понадобится словарь переменных, [вот он]\
        (http://telegra.ph/Slovar-peremennyh-v-sozdanii-tem-na-androide-09-29). \
        Ещё всегда можно спросить @snejugal, если что-то непонятно.\n\n\

        *В России блокируют Телеграм. Для обхода блокировки необходимо \
        поставить прокси — это просто и не займёт много времени.* \
        [Вот, как это сделать](https://t.me/themesrussia/10676).",

        see_you = if is_plural { "вас" } else { "тебя" },
        you_want = if is_plural { "вы хотите" } else { "ты хочешь" },
        do_not_know = if is_plural { "знаете" } else { "знаешь" },
        read = if is_plural { "прочитайте" } else { "прочитай" },
        you_need = if is_plural { "вам" } else { "тебе" },
    )
}

pub fn audio_warning(user: &User) -> String {
    format!(
        "{}, песня классная, но эта группа не про музыку.",
        mention_user(user)
    )
}

pub fn voice_warning(user: &User) -> String {
    format!(
        "Эй, {}, никаких голосовых! Не для того письменность изобрели.",
        mention_user(user)
    )
}

pub fn location_warning(user: &User) -> String {
    format!(
        "Эй, {}, не надо здесь локаций.",
        mention_user(user)
    )
}

pub fn venue_warning(user: &User) -> String {
    location_warning(user)
}

pub fn contact_warning(user: &User) -> String {
    format!(
        "{}, не отправляй здесь контакты.",
        mention_user(user)
    )
}

pub fn game_warning(user: &User) -> String {
    format!(
        "{}, твоя игра конечно классная, но эта группа не про игры.",
        mention_user(user)
    )
}

pub fn invoice_warning(user: &User) -> String {
    format!(
        "{}, вещь крутая, но эта группа — для обсуждения тем, поэтому удалил.",
        mention_user(user)
    )
}

pub fn video_note_warning(user: &User) -> String {
    format!(
        "{}, интересно конечно, но эта группа — для обсуждения тем, поэтому \
         удалил.",
        mention_user(user)
    )
}

pub fn document_warning(user: &User) -> String {
    format!(
        "Хе-хей, {}, здесь нельзя такие файлы постить. Только темы.",
        mention_user(user)
    )
}

pub fn last_warning(user: &User) -> String {
    format!(
        "{}, предупреждаю — ты слишком много раз нарушил мои правила за \
         последние 5 минут (отправил много стикеров, голосовых, локаций \
         и т.д.). Повторится ещё раз — запрещу тебе писать здесь в течение \
         суток.",
        mention_user(user)
    )
}

pub fn restriction(user: &User) -> String {
    format!(
        "{}, я предупрежал тебя, но ты все равно нарушаешь мои правила. Я \
         запретил тебе писать здесь, однако ты можешь читать все сообщения. \
         Через 24 часа ты снова сможешь писать тут.\n\n\

         Впредь не нарушай правила, иначе админы могут забанить тебя навсегда.",
        mention_user(user)
    )
}
