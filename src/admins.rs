use super::{
    groups::{get_group, themes_russia, Group},
    utils::mention_user,
    SELF_ID,
};
use std::sync::{Arc, Mutex};
use tbot::{
    connectors::Https,
    event_loop::EventLoop,
    prelude::*,
    types::{
        chat,
        parameters::Text,
        user::{self, User},
    },
    Bot,
};

const ADMIN_CALL: &str = "@admin";
const DEDUWKA_GG_ID: user::Id = user::Id(488659114);
const IGNORE: [user::Id; 2] = [SELF_ID, DEDUWKA_GG_ID];

#[derive(Debug, Clone)]
pub struct Admins {
    pub themes_russia: Arc<Vec<User>>,
}

fn get_group_admins(
    bot: &Bot<Https>,
    group_id: chat::Id,
) -> impl Future<Item = Vec<User>, Error = ()> {
    bot.get_chat_administrators(group_id)
        .into_future()
        .map(|admins| {
            admins
                .into_iter()
                .filter(|admin| !IGNORE.contains(&admin.user.id))
                .map(|admin| admin.user)
                .collect()
        })
        .map_err(|err| {
            dbg!(err);
        })
}

pub fn get_all_admins(bot: &Bot<Https>) -> Admins {
    let admins = Arc::new(Mutex::new(None));
    let on_ok = Arc::clone(&admins);
    let future = get_group_admins(bot, themes_russia::ID).map(move |admins| {
        *on_ok.lock().unwrap() = Some(Admins {
            themes_russia: Arc::new(admins),
        });
    });

    tbot::run(future);

    Arc::try_unwrap(admins).unwrap().into_inner().unwrap().unwrap()
}

pub fn init(bot: &mut EventLoop<Https>, admins: Admins) {
    bot.text(move |context| {
        let is_admin_call = context
            .text
            .value
            .get(..ADMIN_CALL.len())
            .map(|x| x.eq_ignore_ascii_case(ADMIN_CALL));
        if is_admin_call != Some(true) {
            return;
        }

        let group = match get_group(context.chat.id) {
            Some(group) => group,
            None => return,
        };

        let admins = match group {
            Group::ThemesRussia => Arc::clone(&admins.themes_russia),
        };

        let mentions: Vec<_> = admins.iter().map(mention_user).collect();
        let admins = mentions.join(" ");

        let mut reply = context.send_message_in_reply(Text::markdown(&admins));

        if let Some(reply_to) = &context.reply_to {
            reply = reply.reply_to_message_id(reply_to.id);
        }

        let reply = reply.into_future().map_err(|err| {
            dbg!(err);
        });

        tbot::spawn(reply);
    });
}
