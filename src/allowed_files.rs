use super::{
    admins::Admins,
    groups::get_group,
    utils::is_admin_in_chat,
    warnings::{Soundness, WarnedUsers},
};
use std::{path::Path, time::Duration};
use tbot::{connectors::Https, event_loop::EventLoop, prelude::*};

const ALLOWED_FILES: [&str; 18] = [
    // Themes
    "attheme",
    "tdesktop-theme",
    "tdesktop-palette",
    "attheme-editor",
    "pallete",
    "tgx-theme",
    // Images
    "png",
    "apng",
    "bmp",
    "gif",
    "jpg",
    "jpeg",
    "psd",
    "tiff",
    "webp",
    // Gifs
    "mp4",
    // Localizations
    "strings",
    "xml",
];

pub fn init(
    bot: &mut EventLoop<Https>,
    admins: Admins,
    warned_users: WarnedUsers,
) {
    bot.document(move |context| {
        let from = match &context.from {
            Some(user) => user,
            None => return,
        };

        if is_admin_in_chat(&context.chat, from, &admins) {
            return;
        }

        let group = match get_group(context.chat.id) {
            Some(group) => group,
            None => return,
        };

        let filename = match &context.document.file_name {
            Some(name) => name,
            None => return,
        };

        let path = Path::new(&filename);
        let extension: String = match path.extension() {
            Some(extension) => extension.to_string_lossy().to_string(),
            None => "".into(),
        };

        if !ALLOWED_FILES.contains(&extension.as_str()) {
            let warn_result = warned_users.warn_user(context.chat.id, &from);

            warn_result.process(
                &context.bot,
                context.chat.id,
                &from,
                group,
                Duration::from_secs(3600),
                Soundness::Sound(&group.document_warning(&from)),
            );

            let delete =
                context.delete_this_message().into_future().map_err(|err| {
                    dbg!(err);
                });

            tbot::spawn(delete);
        }
    });
}
