use tbot::types::user;

mod admins;
mod allowed_files;
mod group_management;
mod groups;
mod inline;
mod new_members;
mod prohibited_messages;
mod prohibited_words;
mod utils;
mod warnings;

const SELF_ID: user::Id = user::Id(490726923);

fn main() {
    let bot = tbot::bot!("BOT_TOKEN");
    let admins = admins::get_all_admins(&bot);
    let mut bot = bot.event_loop();

    let warned_users = warnings::WarnedUsers::new();

    admins::init(&mut bot, admins.clone());
    inline::init(&mut bot);
    new_members::init(&mut bot);
    prohibited_words::init(&mut bot);
    prohibited_messages::init(&mut bot, admins.clone(), warned_users.clone());
    allowed_files::init(&mut bot, admins.clone(), warned_users.clone());
    group_management::init(&mut bot, admins.clone(), warned_users.clone());

    bot.polling().start();
}
