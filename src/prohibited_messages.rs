use super::{
    admins::Admins,
    groups::get_group,
    utils::is_admin_in_chat,
    warnings::{Soundness, WarnedUsers},
};
use std::time::Duration;
use tbot::{
    connectors::Https,
    event_loop::EventLoop,
    prelude::*,
    types::{chat, message, User},
    Bot,
};

fn delete_and_warn(
    bot: &Bot<Https>,
    chat: chat::Id,
    message_id: message::Id,
    user: &User,
    warned_users: &WarnedUsers,
    warn_message: String,
) {
    let group = match get_group(chat) {
        Some(group) => group,
        None => return,
    };

    let warn_result = warned_users.warn_user(chat, user);
    warn_result.process(
        bot,
        chat,
        user,
        group,
        Duration::from_secs(3600),
        Soundness::Sound(&warn_message),
    );

    let delete =
        bot.delete_message(chat, message_id).into_future().map_err(|err| {
            dbg!(err);
        });

    tbot::spawn(delete);
}

macro_rules! warn_handler {
    (
        admins: $admins:ident,
        warned_users: $users:ident,
        warning: $warning:ident,
    ) => {{
        move |context| {
            let from = match &context.from {
                Some(user) => user,
                None => return,
            };

            if is_admin_in_chat(&context.chat, from, &$admins) {
                return;
            }

            let group = match get_group(context.chat.id) {
                Some(group) => group,
                None => return,
            };

            delete_and_warn(
                &context.bot,
                context.chat.id,
                context.message_id,
                &from,
                &$users,
                group.$warning(&from),
            );
        }
    }};
}

pub fn init(
    bot: &mut EventLoop<Https>,
    admins: Admins,
    warned_users: WarnedUsers,
) {
    let on_audio = admins.clone();
    let audio_warn = warned_users.clone();
    bot.audio(warn_handler!(
        admins: on_audio,
        warned_users: audio_warn,
        warning: audio_warning,
    ));

    let on_voice = admins.clone();
    let voice_warn = warned_users.clone();
    bot.voice(warn_handler!(
        admins: on_voice,
        warned_users: voice_warn,
        warning: voice_warning,
    ));

    let on_location = admins.clone();
    let location_warn = warned_users.clone();
    bot.location(warn_handler!(
        admins: on_location,
        warned_users: location_warn,
        warning: location_warning,
    ));

    let on_venue = admins.clone();
    let venue_warn = warned_users.clone();
    bot.venue(warn_handler!(
        admins: on_venue,
        warned_users: venue_warn,
        warning: venue_warning,
    ));

    let on_contact = admins.clone();
    let contact_warn = warned_users.clone();
    bot.contact(warn_handler!(
        admins: on_contact,
        warned_users: contact_warn,
        warning: contact_warning,
    ));

    let on_game = admins.clone();
    let game_warn = warned_users.clone();
    bot.game(warn_handler!(
        admins: on_game,
        warned_users: game_warn,
        warning: game_warning,
    ));

    let on_invoice = admins.clone();
    let invoice_warn = warned_users.clone();
    bot.invoice(warn_handler!(
        admins: on_invoice,
        warned_users: invoice_warn,
        warning: invoice_warning,
    ));

    let on_video_note = admins.clone();
    let video_note_warn = warned_users.clone();
    bot.video_note(warn_handler!(
        admins: on_video_note,
        warned_users: video_note_warn,
        warning: video_note_warning,
    ));

    let on_sticker = admins.clone();
    let sticker_warn = warned_users.clone();
    bot.sticker(move |context| {
        let from = match &context.from {
            Some(user) => user,
            None => return,
        };

        if is_admin_in_chat(&context.chat, from, &on_sticker) {
            return;
        }

        let group = match get_group(context.chat.id) {
            Some(group) => group,
            None => return,
        };

        let warn_result = sticker_warn.warn_user(context.chat.id, from);
        warn_result.process(
            &context.bot,
            context.chat.id,
            from,
            group,
            Duration::from_secs(3600),
            Soundness::Silent,
        );
    })
}
