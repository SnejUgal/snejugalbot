use tbot::{
    connectors::Https,
    event_loop::EventLoop,
    prelude::*,
    types::{
        inline_query::{self, result::Article},
        input_message_content::Text,
        parameters::{Text as ParseMode, WebPagePreviewState},
    },
};

const NEW_MEMBERS_TITLE: &str = "New members";
const NEW_MEMBERS: &str = "\
    *Welcome new members!*\n\n\

    Please make sure to read \
    [the pinned message](https://t.me/AndroidThemesGroup/316520) \
    and remember:\n\
    — to use only *English*;\n\
    — *not* to *advertise*;\n\
    — *not* to *spam* _(also go easy on media, gifs etc.)_;\n\
    — *not* to post *stickers* (they are removed automatically).\n\n\

    This is a group for *sharing*, *discussing* and *creating themes*. Please \
    refer to @snowballfight or @publictestgroup for general chit-chat.\n\n\

    *Thanks for your attention and enjoy your stay!*";

const OTHER_LANGUAGES_TITLE: &str = "Other languages";
const OTHER_LANGUAGES: &str = "\
    *Other languages*\n\n\

    Desktop and Android:\n\
    — [🇬🇧 English](http://t.me/themesandroidchat)\n\
    — [🇪🇸 Spanish](http://t.me/ThemesSpanish)\n\
    — [🇷🇴 Romanian](http://t.me/ThemesRO)\n\
    — [🇺🇿 Uzbek](http://t.me/themesuzb)\n\
    — [🇮🇳 Indian](http://t.me/AndroidThemesINGroup)\n\
    — [🇮🇹 Italian](http://t.me/TGThemesITA)\n\
    — [🇵🇹 Portuguese](http://t.me/ThemesinPortuguese)\n\
    — [🇷🇺 Russian](http://t.me/ThemesRussia)\n\
    — [🇩🇪 German](http://t.me/joinchat/EppBI0oGxkQytuPkQyoYMw)\n\n\

    Android:\n\
    — [🇫🇷 French](http://t.me/AndroidThemeFrance)\n\
    — [🇮🇷 Persian](http://t.me/joinchat/BBbCTE3b4ZTOefO6XCQLZw)";

const OFF_TOPIC_GROUPS_TITLE: &str = "Off-topic groups";
const OFF_TOPIC_GROUPS: &str = "\
    *Off-topic groups*\n\n\

    — @publictestgroup\n\
    — @snowballfight";

const EDITORS_TITLE: &str = "Editors";
const EDITORS: &str = "\
    *Editors*\n\n\

    — [SnejUgal's](https://snejugal.ru/attheme-editor)\n\
    — [YouTwitFace's](https://lungers.com/attheme)\n\
    — [Daniele Nicotra's]\
      (http://danicotra.getenjoyment.net/attheme-etk/attheme-etk.htm) \
      ([Introduction](http://danicotra.getenjoyment.net/attheme-etk))";

const TOOLS_TITLE: &str = "Tools";
const TOOLS: &str = "\
    *Tools*\n\n\

    — [Compare .attheme's](https://compare-atthemes.snejugal.ru)\n\
    — @atthemeglossarybot\n\
    — @atthemeimagebot\n\
    — @atthemeeditorbot\n\
    — @testatthemebot\n\
    — @randomatthemebot\n\
    — @themepreviewbot\n\
    — @themesporterbot\n\
    — @invertatthemebot";

pub fn init(bot: &mut EventLoop<Https>) {
    let new_members = Text::new(ParseMode::markdown(NEW_MEMBERS))
        .web_page_preview(WebPagePreviewState::Disabled);
    let new_members = Article::new(NEW_MEMBERS_TITLE, new_members);

    let other_languages = Text::new(ParseMode::markdown(OTHER_LANGUAGES))
        .web_page_preview(WebPagePreviewState::Disabled);
    let other_languages = Article::new(OTHER_LANGUAGES_TITLE, other_languages);

    let off_topic_groups = Text::new(ParseMode::markdown(OFF_TOPIC_GROUPS));
    let off_topic_groups =
        Article::new(OFF_TOPIC_GROUPS_TITLE, off_topic_groups);

    let editors = Text::new(ParseMode::markdown(EDITORS))
        .web_page_preview(WebPagePreviewState::Disabled);
    let editors = Article::new(EDITORS_TITLE, editors);

    let tools = Text::new(ParseMode::markdown(TOOLS))
        .web_page_preview(WebPagePreviewState::Disabled);
    let tools = Article::new(TOOLS_TITLE, tools);

    let results = [
        inline_query::Result::new("0", new_members),
        inline_query::Result::new("1", other_languages),
        inline_query::Result::new("2", off_topic_groups),
        inline_query::Result::new("3", editors),
        inline_query::Result::new("4", tools),
    ];

    bot.inline(move |context| {
        let answer = context.answer(&results).into_future().map_err(|err| {
            dbg!(err);
        });

        tbot::spawn(answer);
    });
}
