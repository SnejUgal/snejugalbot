use tbot::types::{chat, User};

pub mod themes_russia;

pub const GROUPS: [chat::Id; 1] = [themes_russia::ID];

#[derive(Clone, Copy)]
pub enum Group {
    ThemesRussia,
}

pub fn get_group(id: chat::Id) -> Option<Group> {
    match id {
        themes_russia::ID => Some(Group::ThemesRussia),
        _ => None,
    }
}

macro_rules! locale {
    ($self:ident, $method:ident, $($argument:ident$(,)?)*) => {
        match $self {
            $crate::groups::Group::ThemesRussia => {
                themes_russia::$method($($argument,)*)
            }
        }
    };
}

impl Group {
    pub fn welcome(self, is_plural: bool) -> String {
        locale!(self, welcome, is_plural)
    }

    pub fn audio_warning(self, user: &User) -> String {
        locale!(self, audio_warning, user)
    }

    pub fn voice_warning(self, user: &User) -> String {
        locale!(self, voice_warning, user)
    }

    pub fn location_warning(self, user: &User) -> String {
        locale!(self, location_warning, user)
    }

    pub fn venue_warning(self, user: &User) -> String {
        locale!(self, venue_warning, user)
    }

    pub fn contact_warning(self, user: &User) -> String {
        locale!(self, contact_warning, user)
    }

    pub fn game_warning(self, user: &User) -> String {
        locale!(self, game_warning, user)
    }

    pub fn invoice_warning(self, user: &User) -> String {
        locale!(self, invoice_warning, user)
    }

    pub fn video_note_warning(self, user: &User) -> String {
        locale!(self, video_note_warning, user)
    }

    pub fn document_warning(self, user: &User) -> String {
        locale!(self, document_warning, user)
    }

    pub fn last_warning(self, user: &User) -> String {
        locale!(self, last_warning, user)
    }

    pub fn restriction(self, user: &User) -> String {
        locale!(self, restriction, user)
    }
}
