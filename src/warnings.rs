use super::groups::Group;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::time::{Duration, Instant, SystemTime};
use tbot::{
    connectors::Https,
    types::{
        chat,
        parameters::{NotificationState::Disabled, Text},
        user::{self, User},
    },
    Bot,
};
use tokio::{prelude::*, timer::Delay};

#[derive(Clone)]
pub struct WarnedUsers(Arc<Mutex<HashMap<(chat::Id, user::Id), u8>>>);

pub enum Soundness<'a> {
    Silent,
    Sound(&'a str),
}

pub enum WarnResult {
    Warn,
    LastWarn,
    Restrict,
}

impl WarnedUsers {
    pub fn new() -> Self {
        let users = Arc::new(Mutex::new(HashMap::new()));
        Self(users)
    }

    pub fn warn_user(&self, chat: chat::Id, user: &User) -> WarnResult {
        let chat_id = chat;
        let user_id = user.id;

        let users = self.0.clone();
        let mut users = users.lock().unwrap();
        let times = users.entry((chat_id, user_id)).or_insert(0);
        *times += 1;

        let when = Instant::now() + Duration::from_secs(60 * 5);
        let users = self.0.clone();
        let remove_warning = Delay::new(when)
            .map(move |_| {
                let mut users = users.lock().unwrap();
                users.entry((chat_id, user_id)).and_modify(|times| *times -= 1);
            })
            .map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(remove_warning);

        if *times >= 5 {
            WarnResult::Restrict
        } else if *times == 4 {
            WarnResult::LastWarn
        } else {
            WarnResult::Warn
        }
    }
}

impl WarnResult {
    pub fn process(
        self,
        bot: &Bot<Https>,
        chat: chat::Id,
        user: &User,
        group: Group,
        date: Duration,
        soundness: Soundness,
    ) {
        match self {
            WarnResult::Warn => match soundness {
                Soundness::Sound(message) => {
                    let warn = bot
                        .send_message(chat, Text::markdown(message))
                        .notification(Disabled)
                        .into_future()
                        .map_err(|err| {
                            dbg!(err);
                        });

                    tbot::spawn(warn);
                }
                Soundness::Silent => return,
            },
            WarnResult::LastWarn => {
                let last_warn = bot
                    .send_message(
                        chat,
                        Text::markdown(&group.last_warning(user)),
                    )
                    .notification(Disabled)
                    .into_future()
                    .map_err(|err| {
                        dbg!(err);
                    });

                tbot::spawn(last_warn);
            }
            WarnResult::Restrict => {
                let restrict_message = bot
                    .send_message(
                        chat,
                        Text::markdown(&group.restriction(user)),
                    )
                    .notification(Disabled)
                    .into_future()
                    .map_err(|err| {
                        dbg!(err);
                    });

                let date = {
                    (SystemTime::now() + date)
                        .duration_since(SystemTime::UNIX_EPOCH)
                        .unwrap()
                        .as_secs()
                };

                let restrict = bot
                    .restrict_chat_member(chat, user.id)
                    .can_send_messages(false)
                    .until_date(date as i64)
                    .into_future()
                    .map_err(|err| {
                        dbg!(err);
                    });

                tbot::spawn(restrict.join(restrict_message));
            }
        }
    }
}
