use super::groups::{get_group, Group};
use std::sync::{Arc, Mutex};
use tbot::{
    connectors::Https,
    event_loop::EventLoop,
    prelude::*,
    types::{
        parameters::{NotificationState, Text, WebPagePreviewState},
        user,
    },
};

const ALLOWED_BOTS: [user::Id; 3] = [
    user::Id(458805360), // @snejugalbot
    user::Id(419395725), // @themepreviewbot
    user::Id(478137758), // @atthemeglossarybot
];

pub fn init(bot: &mut EventLoop<Https>) {
    let themes_russia_last_message = Arc::new(Mutex::new(None));

    bot.new_members(move |context| {
        let leave = || {
            let leave = context.leave_chat().into_future().map_err(|err| {
                dbg!(err);
            });

            tbot::spawn(leave);
        };

        let inviter_id = match &context.from {
            Some(from) => from.id,
            None => {
                // If `from` is `None`, then the update comes from a channel.
                // This bot isn't for channels.
                leave();
                return;
            }
        };

        let group = match get_group(context.chat.id) {
            Some(group) => group,
            None => {
                leave();
                return;
            }
        };

        let last_message = match group {
            Group::ThemesRussia => Arc::clone(&themes_russia_last_message),
        };

        let mut is_bot_inviter_kicked = false;

        for member in &context.members {
            if member.is_bot && !ALLOWED_BOTS.contains(&member.id) {
                let kick_bot = context
                    .kick_chat_member(member.id)
                    .into_future()
                    .map_err(|err| {
                        dbg!(err);
                    });

                tbot::spawn(kick_bot);

                if !is_bot_inviter_kicked {
                    is_bot_inviter_kicked = true;

                    let kick_inviter = context
                        .kick_chat_member(inviter_id)
                        .into_future()
                        .map_err(|err| {
                            dbg!(err);
                        });

                    tbot::spawn(kick_inviter);
                }
            }
        }

        if is_bot_inviter_kicked {
            return;
        }

        if let Some(last_message) = *last_message.lock().unwrap() {
            let delete_last_message = context
                .delete_message(last_message)
                .into_future()
                .map_err(|err| {
                    dbg!(err);
                });

            tbot::spawn(delete_last_message);
        }

        let is_plural = context.members.len() > 1;
        let text = group.welcome(is_plural);
        let message = context
            .send_message_in_reply(Text::markdown(&text))
            .notification(NotificationState::Disabled)
            .web_page_preview(WebPagePreviewState::Disabled)
            .into_future()
            .map(move |message| {
                *last_message.lock().unwrap() = Some(message.id);
            })
            .map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(message);
    });
}
