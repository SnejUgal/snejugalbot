use tbot::{
    connectors::Https, event_loop::EventLoop, prelude::*,
    types::parameters::NotificationState,
};

const PROHIBITED_WORDS: [&str; 74] = [
    "навальн",
    "ональны",
    "онально",
    "овальн",
    "путин",
    "хуйл",
    "зюганов",
    "грудинин",
    "трамп",
    "ким чен ын",
    "обам",
    "порошенк",
    "клинтон",
    "миронов",
    "собчак",
    "медведев",
    "яшин",
    "асад",
    "игил",
    "шейн",
    "хохол",
    "хохл",
    "венедиктов",
    "жириновск",
    "фельгенгауэр",
    "мутко",
    "кашин",
    "едро",
    "едра",
    "едру",
    "едром",
    "саакашвили",
    "ройзман",
    "вилсаком",
    "быть или",
    "камикадзе",
    "бэдкомидиан",
    "badcomedian",
    "песков",
    "милов",
    "сталин",
    "шаведдинов",
    "адагамов",
    "арам ашотыч",
    "габрелянов",
    "сирия",
    "lifenews",
    "@lighthemes",
    "бундестаг",
    "серебренников",
    "будет лучше",
    "порн",
    "иран",
    "ирак",
    "незыгар",
    "лентач",
    "соловьев",
    "соловьёв",
    "киселёв",
    "киселев",
    "мдк",
    "2к",
    "rtvi",
    "ляскин",
    "прокопенк",
    "чапман",
    "bitcoin",
    "биткоин",
    "шагинян",
    "дружк",
    "тнт",
    "стс",
    "олимпиад",
    "гнойн",
];

pub fn init(bot: &mut EventLoop<Https>) {
    bot.text(|context| {
        let text = context.text.value.to_lowercase();

        for prohibited_word in &PROHIBITED_WORDS[..] {
            if text.contains(prohibited_word) {
                let reply = context
                    .send_message_in_reply("👀")
                    .notification(NotificationState::Disabled)
                    .into_future()
                    .map_err(|err| {
                        dbg!(err);
                    });

                tbot::spawn(reply);
                return;
            }
        }
    });
}
