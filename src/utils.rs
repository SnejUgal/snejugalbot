use super::{
    admins::Admins,
    groups::{get_group, Group},
};
use std::sync::Arc;
use tbot::{
    connectors::Https,
    contexts,
    types::{Chat, User},
};

pub fn mention_user(user: &User) -> String {
    match &user.username {
        Some(username) => format!("@{}", username),
        None => format!("[{}](tg://user?id={})", user.first_name, user.id),
    }
}

pub fn is_from_admin(context: &contexts::Text<Https>, admins: &Admins) -> bool {
    match &context.from {
        Some(user) => is_admin_in_chat(&context.chat, &user, admins),
        None => false,
    }
}

pub fn is_admin_in_chat(chat: &Chat, user: &User, admins: &Admins) -> bool {
    let group = match get_group(chat.id) {
        Some(group) => group,
        None => return false,
    };

    let admins = match group {
        Group::ThemesRussia => Arc::clone(&admins.themes_russia),
    };

    admins.contains(user)
}
