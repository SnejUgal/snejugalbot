use super::{
    admins::Admins,
    groups::get_group,
    utils::{is_admin_in_chat, is_from_admin},
    warnings::{Soundness::Silent, WarnedUsers},
};
use std::time::{Duration, SystemTime};
use tbot::{
    connectors::Https,
    contexts,
    event_loop::EventLoop,
    prelude::*,
    types::{input_file::Sticker, parameters::NotificationState::Disabled},
};

fn restrict(
    context: &contexts::Text<Https>,
    admins: &Admins,
    warned_users: &WarnedUsers,
) {
    let is_from_admin = is_from_admin(&context, &admins);
    let from = match &context.from {
        Some(user) => user,
        None => return,
    };

    let reply_to = match &context.reply_to {
        Some(message) => message,
        None => {
            if is_from_admin {
                return;
            }

            let until_date = {
                (SystemTime::now() + Duration::from_secs(3600))
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_secs()
            };
            let restrict_user = context
                .restrict_chat_member(from.id)
                .can_send_messages(false)
                .until_date(until_date as i64)
                .into_future()
                .map_err(|err| {
                    dbg!(err);
                });

            tbot::spawn(restrict_user);
            return;
        }
    };

    let user_to_restrict = match &reply_to.from {
        Some(user_to_restrict) => user_to_restrict,
        None => return,
    };

    if is_from_admin {
        if is_admin_in_chat(&context.chat, user_to_restrict, admins) {
            return;
        }

        let restrict_user = context
            .restrict_chat_member(user_to_restrict.id)
            .can_send_messages(false)
            .into_future()
            .map_err(|err| {
                dbg!(err);
            });
        let delete_command =
            context.delete_this_message().into_future().map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(restrict_user.join(delete_command));
    } else {
        let reply = context
            .send_sticker_in_reply(Sticker::bytes(include_bytes!(
                "../assets/restrict_command.webp"
            )))
            .notification(Disabled)
            .into_future()
            .map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(reply);

        let warn_result = warned_users.warn_user(context.chat.id, from);
        let group = match get_group(context.chat.id) {
            Some(group) => group,
            None => return,
        };
        warn_result.process(
            &context.bot,
            context.chat.id,
            from,
            group,
            Duration::from_secs(3600),
            Silent,
        )
    }
}

pub fn init(
    bot: &mut EventLoop<Https>,
    admins: Admins,
    warned_users: WarnedUsers,
) {
    let on_delete = admins.clone();
    bot.command("delete", move |context| {
        let reply_to = match &context.reply_to {
            Some(message) => message,
            None => return,
        };

        if !is_from_admin(&context, &on_delete) {
            return;
        }

        let delete_message =
            context.delete_message(reply_to.id).into_future().map_err(|err| {
                dbg!(err);
            });
        let delete_command =
            context.delete_this_message().into_future().map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(delete_message.join(delete_command));
    });

    let on_ban = admins.clone();
    bot.command("ban", move |context| {
        let reply_to = match &context.reply_to {
            Some(message) => message,
            None => return,
        };

        let user_to_ban = match &reply_to.from {
            Some(user_to_ban) => user_to_ban,
            None => return,
        };

        if !is_from_admin(&context, &on_ban) {
            return;
        }

        let kick = context
            .kick_chat_member(user_to_ban.id)
            .into_future()
            .map_err(|err| {
                dbg!(err);
            });
        let delete_command =
            context.delete_this_message().into_future().map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(kick.join(delete_command));
    });

    let on_restrict = admins.clone();
    let restrict_warn = warned_users.clone();
    bot.command("restrict", move |context| {
        restrict(context, &on_restrict, &restrict_warn)
    });

    let readonly_warn = warned_users.clone();
    let on_readonly = admins.clone();
    bot.command("readonly", move |context| {
        restrict(context, &on_readonly, &readonly_warn)
    });

    let on_pin = admins.clone();
    bot.command("pin", move |context| {
        let reply_to = match &context.reply_to {
            Some(message) => message,
            None => return,
        };

        if !is_from_admin(&context, &on_pin) {
            return;
        }

        let pin = context
            .bot
            .pin_chat_message(context.chat.id, reply_to.id)
            .notification(Disabled)
            .into_future()
            .map_err(|err| {
                dbg!(err);
            });
        let delete_command =
            context.delete_this_message().into_future().map_err(|err| {
                dbg!(err);
            });

        tbot::spawn(pin.join(delete_command));
    })
}
